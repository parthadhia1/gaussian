format longE
m = 9; n = 9; 
value = 1000000;
sigma = 1; 
while value > 1 
[h1, h2] = meshgrid(-(m-1)/2:(m-1)/2, -(n-1)/2:(n-1)/2);
hg = exp( - (h1.^2+h2.^2) / (2*sigma^2)); 
h = hg ./ sum(hg(:));
h = fix(h.*value)./value;
rgbImage = imread('peppers.png');
redChannel = rgbImage(:, :, 1);
greenChannel = rgbImage(:, :, 2);
blueChannel = rgbImage(:, :, 3);
redChannel = filter2(h, redChannel);
redChannel = uint8(redChannel);
blueChannel = filter2(h,blueChannel);
blueChannel = uint8(blueChannel);
greenChannel = filter2(h, greenChannel);
greenChannel = uint8(greenChannel);
% Recombine separate color channels into an RGB image.
I = cat(3, redChannel, greenChannel, blueChannel);
if value == 1000000
    imwrite(I,'6decimal.bmp');
elseif value == 100000
    imwrite(I,'5decimal.bmp');
elseif value == 10000
    imwrite(I,'4decimal.bmp');
elseif value == 1000
    imwrite(I,'3decimal.bmp');
elseif value == 100 
    imwrite(I,'2decimal.bmp');
elseif value == 10
    imwrite(I,'1decimal.bmp');
else
    imwrite(I,'0decimal.bmp');
end 
value = value / 10; 
end