format longE
m = 9; n = 9; 
value = 1000000;
sigma = 3; 
while value > 1 
[h1, h2] = meshgrid(-(m-1)/2:(m-1)/2, -(n-1)/2:(n-1)/2);
hg = exp( - (h1.^2+h2.^2) / (2*sigma^2)); 
h = hg ./ sum(hg(:));
h = fix(h.*value)./value;
L = imread('checks.jpg');
L = rgb2gray(L);
I = filter2(h, L);
I = uint8(I);
if value == 1000000
    imwrite(I,'6decimal.bmp');
elseif value == 100000
    imwrite(I,'5decimal.bmp');
elseif value == 10000
    imwrite(I,'4decimal.bmp');
elseif value == 1000
    imwrite(I,'3decimal.bmp');
elseif value == 100 
    imwrite(I,'2decimal.bmp');
elseif value == 10
    imwrite(I,'1decimal.bmp');
else
    imwrite(I,'0decimal.bmp');
end 
value = value / 10; 
end