module Complete_MIPS(CLK, RST, HALT, SW, BT, Cathode, Anode);
  input CLK, RST, HALT;
  input [2:0] SW;
  input [1:0] BT;
  output [6:0] Cathode;
  output [3:0] Anode;

  wire CS, WE;
  wire [31:0] ADDR, Mem_Bus;

  MIPS CPU(CLK, RST, HALT, CS, WE, ADDR, Mem_Bus, SW, BT, Cathode, Anode);
  Memory MEM(CS, WE, CLK, ADDR, Mem_Bus);
endmodule