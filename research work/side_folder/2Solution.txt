R4 = 0x70000000
R5 = 0x7FFF0000
operation2 LUI constant: 0x1000

Case (SW):
  0:  R3 = 0x37FF9000, R2 = 0x00000000
  1:  R2 = 0xEFFF0000
  2:  R2 = 0x10000000
  3:  R2 = 0x0000FFFE
  4:  R2 = 0x00000070
  5:  R2 = 0xFFFE0000
  6:  R2 = 0x00000000
