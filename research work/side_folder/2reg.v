module REG(CLK, RegW, DR, SR1, SR2, Reg_In, ReadReg1, ReadReg2, Switch_Input, Button_Input, cat, an);
  input CLK;
  input RegW;
  input [2:0] Switch_Input;
  input [1:0] Button_Input;
  input [4:0] DR;
  input [4:0] SR1;
  input [4:0] SR2;
  input [31:0] Reg_In;
  output reg [31:0] ReadReg1;
  output reg [31:0] ReadReg2;
  output [6:0] cat;
  output [3:0] an;

  reg [31:0] REG [0:31];
  reg [15:0] value;
  wire ssd_en = 1'b1;

  initial begin
    ReadReg1 = 0;
    ReadReg2 = 0;
    value = 0;
  end

  //select what content to output to ssd
  always @(*)
  begin
    case (Switch_Input)
      0: begin
        case (Button_Input)
          0: value = REG[2][15:0];
          1: value = REG[2][31:16];
          2: value = REG[3][15:0];
          3: value = REG[3][31:16];
        endcase
      end
      1, 2, 3, 4, 5, 6, 7: begin
        case (Button_Input)
          0, 2: value = REG[2][15:0];
          1, 3: value = REG[2][31:16];
        endcase
      end
    endcase
  end

  always @(posedge CLK)
  begin
    if(RegW == 1'b1) begin
      REG[DR] <= Reg_In[31:0];
    end
    REG[1] <= {29'b0, Switch_Input}; // load lowest 3 bits in $1 with the switch inputs
    ReadReg1 <= REG[SR1];
    ReadReg2 <= REG[SR2];
  end

  SSD ssd1(CLK, ssd_en, value, cat, an);
endmodule
