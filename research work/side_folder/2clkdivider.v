module ClkDivider(ClkIn, Scale, ClkOut);
  input ClkIn;
  input [31:0] Scale;
  output reg ClkOut;

  reg [31:0] counter;
  initial begin
    ClkOut <= 1'b0;
    counter <= 32'd0;
  end

  always @(posedge ClkIn)
  begin
    if (counter == (Scale>>1)-1) begin
      counter <= 32'd0;
      ClkOut <= ~ClkOut;
    end
    else begin
      counter <= counter + 32'd1;
    end
  end
endmodule
