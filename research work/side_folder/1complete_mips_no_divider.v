module Complete_MIPS_no_divider(CLK, RST, HALT, LED, A_Out, D_Out);
	input CLK, RST, HALT;
	output [7:0] LED;
	output [31:0] A_Out;
	output [31:0] D_Out;

	wire CS, WE;
	wire [31:0] ADDR, Mem_Bus;

	assign A_Out = ADDR;
	assign D_Out = Mem_Bus;

	MIPS CPU(CLK, RST, HALT, CS, WE, ADDR, Mem_Bus, LED);
	Memory MEM(CS, WE, CLK, ADDR, Mem_Bus);
endmodule
