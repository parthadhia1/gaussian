  `define FREQ_BusClk   100000000 // 10 ns
  `define FREQ_SSD           500 // 2 ms

module SSD(BusClk, En, Value, Cathode, Anode);
  input BusClk, En;
  input [15:0] Value;
  output reg [6:0] Cathode;
  output [3:0] Anode;

  wire SSDClk;
  wire [6:0] seg3, seg2, seg1, seg0;
  reg [3:0] sel;

  ClkDivider div2(BusClk, `FREQ_BusClk/`FREQ_SSD, SSDClk);
  Bcd_seven Bcd3(Value[15:12], seg3);
  Bcd_seven Bcd2(Value[11:8], seg2);
  Bcd_seven Bcd1(Value[7:4], seg1);
  Bcd_seven Bcd0(Value[3:0], seg0);

  initial begin
    sel <= 4'b0111;
  end

  always @(posedge SSDClk)
  begin
    if (En) begin
      sel <= {sel[0], sel[3:1]};
    end
  end

  always @(*)
  begin
    if (!En) begin
      Cathode = 7'b1111111; //all off
    end
    else begin
      case(sel)
        //negative logic anodes
        4'b0111: begin Cathode = seg3; end
        4'b1011: begin Cathode = seg2; end
        4'b1101: begin Cathode = seg1; end
        4'b1110: begin Cathode = seg0; end
        default: begin Cathode = 7'b1111111; end
      endcase
    end
  end

  assign Anode = sel;
endmodule

///////////////////////////////////////////////////////////////////////////////

module Bcd_seven(bcd, seven);
  input [3:0] bcd;
  output reg [6:0] seven;

  always @(bcd)
  begin
    case(bcd)
      //negative logic cathodes
      4'b0000: seven = 7'b1000000; //0
      4'b0001: seven = 7'b1111001; //1
      4'b0010: seven = 7'b0100100; //2
      4'b0011: seven = 7'b0110000; //3
      4'b0100: seven = 7'b0011001; //4
      4'b0101: seven = 7'b0010010; //5
      4'b0110: seven = 7'b0000010; //6
      4'b0111: seven = 7'b1111000; //7
      4'b1000: seven = 7'b0000000; //8
      4'b1001: seven = 7'b0010000; //9
      4'b1010: seven = 7'b0001000; //A
      4'b1011: seven = 7'b0000011; //B
      4'b1100: seven = 7'b1000110; //C
      4'b1101: seven = 7'b0100001; //D
      4'b1110: seven = 7'b0000110; //E
      4'b1111: seven = 7'b0001110; //F
      default: seven = 7'b1111111; //off
    endcase
  end
endmodule
