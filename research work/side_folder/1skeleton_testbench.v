module MIPS_Testbench ();
  reg CLK;
  reg RST;
  reg HALT;
  wire LED0, LED1, LED2, LED3, LED4, LED5, LED6, LED7;
  wire [31:0] Mem_Bus;
  wire [31:0] Address;

  initial begin
    CLK <= 1'b0;
    RST <= 1'b1; //reset the processor
  end

	Complete_MIPS_no_divider chip0(CLK, RST, HALT, LED0, LED1, LED2, LED3, LED4, LED5, LED6, LED7, Address, Mem_Bus);

 always
 begin
	#10 CLK = ~CLK;
 end

  always
  begin
    @(posedge CLK);
    // driving reset low here puts processor in normal operating mode
    RST = 1'b0;
    HALT <= 1'b0;
    $display("\nBegin Program");
    /* add your testing code here */
    // you can add in a 'Halt' signal here as well to test Halt operation
    // you will be verifying your program operation using the
    // waveform viewer and/or self-checking operations
    #3000
    HALT = 1'b1; // halt
    $display("\nHalt on");
    #1000
    HALT = 1'b0; // resume
    $display("\nHalt off");
    #3000
    RST = 1'b1; // reset
    $display("\nReset on");
    #500
    RST = 1'b0; // restart
    $display("\nReset off - Restarting Program");
    #2500

    $display("\nTEST COMPLETE");
    $stop;
  end

endmodule
