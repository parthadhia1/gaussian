		andi $0, $0, 0		# initialize $0 to 0
		addi $1, $0, 1		# initialize $1 to 1		
loop:	andi $2, $1, 128	# Is $1[7] == 1?
		bne	 $2, $0, reset	# 		
		sll $1, $1, 1		# left shift once
		j	loop
reset	addi $1, $0, 1		# reset $1 back to 1
		j	loop
		