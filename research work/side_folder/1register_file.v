 module REG(CLK, RegW, DR, SR1, SR2, Reg_In, ReadReg1, ReadReg2, Low8Reg1);
	input CLK;
	input RegW;
	input [4:0] DR;
	input [4:0] SR1;
	input [4:0] SR2;
	input [31:0] Reg_In;
	output reg [31:0] ReadReg1;
	output reg [31:0] ReadReg2;
	output [7:0] Low8Reg1;

	reg [31:0] REG [0:31];

	initial begin
		ReadReg1 = 0;
		ReadReg2 = 0;
	end

	always @(posedge CLK)
	begin
		if(RegW == 1'b1) begin
			REG[DR] <= Reg_In[31:0];
		end
		ReadReg1 <= REG[SR1];
		ReadReg2 <= REG[SR2];
	end

	assign Low8Reg1 = REG[1][7:0]; ///output lowest byte of $1
endmodule
