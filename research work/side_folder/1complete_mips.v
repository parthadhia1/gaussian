module Complete_MIPS(CLK, RST, HALT, LED);
	input CLK, RST, HALT;
	output [7:0] LED;

  wire CS, WE;
	wire [31:0] ADDR, Mem_Bus;

	reg slowClk;
	reg [26:0] counter;

	initial begin
		counter <= 0;
		slowClk <= 0;
	end

  /* 10 Hz clock */
	always @ (posedge CLK) begin
		if(counter == 5000000)
			begin
			counter <= 1;
			slowClk <= ~slowClk; //period = 0.1 seconds
			end
		else begin
			counter <= counter + 1;
		end
	end

	MIPS CPU(slowClk, RST, HALT, CS, WE, ADDR, Mem_Bus, LED);
	Memory MEM(CS, WE, slowClk, ADDR, Mem_Bus);
endmodule
